from gifts import model


def test_family_creation_from_dict():
    # given a dictionary providing a Family content
    family_dict = {"bob": "odnts@dcs.fr", "lea": ""}

    # when it is used to instantiate a Family instance
    family = model.Family.from_dict(family_dict)

    # then expected persons are in the Family instance
    assert family.name == "root"
    for person_name, person_email in family_dict.items():
        assert person_name in family.people
        assert family[person_name].email == person_email
    # ... and there is no sub-family
    assert not family.subs


def test_family_creation_from_dict_with_sub_family():
    # given a dictionary providing a Family content with sub
    family_data = {"bob": "odnts@dcs.fr", "lea": "", "sb1": {"joe": "--"}}

    # when it is used to instantiate a Family instance
    family = model.Family.from_dict(family_data)

    # then the sub-family exists
    assert "sb1" in family.subs
    assert "joe" in family.subs["sb1"]
    assert isinstance(family.subs["sb1"]["joe"], model.Person)
    assert family.subs["sb1"]["joe"].email == "--"

    # ... and the list of all persons in the family contains all people
    assert set(family.all_names.keys()) == {"bob", "lea", "joe"}
    assert set(person.name for person in family.persons) == {
        "bob",
        "lea",
        "joe",
    }


def test_family_creation_with_sub_family():
    # given a sub-family
    joe = model.Person(name="joe", email="blob")
    sub = model.Family(name="sub", people={"joe": joe})

    # when a new family is created
    family = model.Family(name="root", subs={"sub": sub})

    # then it is possible to directly retrieve a person from sub-family
    assert "joe" in family
    assert family["joe"] == joe
    # ... and to retrieve a sub-family using [] access
    assert "sub" in family
    assert family["sub"] == sub
