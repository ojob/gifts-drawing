#: coding: utf-8
"""This module provides a shuffler of email-addresses, sent using direct
call to an SMTP server, so that no-one knows the instructions other got.

Usage
=====

Importer le module `main`, et utiliser l'une des deux fonctions suivantes :

* :py:func:`draw_gifts`: pour faire un tirage aléatoire, puis envoyer un
  mail à chacun des participants ;
* :py:func:`send_reminder`: pour recharger un tirage, puis envoyer un mail
  de relance aux participants.

Les fichiers suivants doivent être fournis :

* un fichier décrivant la famille, au format YAML_, structuré de manière
  hiérarchique, permettant d'instancier :py:class:Family.
* un fichier indiquant les paramètres pour émettre des mails, toujours au
  format YAML, utilisé pour instancier :py:class:Secrets.

.. _YAML: https://yaml.org/

En sortie, un fichier décrivant la ou les boucles de transferts de cadeau
est généré, sous forme d'une liste de listes.


Charte
======

Comme discuté avec Fañch, le code doit :

 * envoyer une relance
 * concerne le cadeau entre frères et sœurs, beaux-frères et belles-sœurs,
   gendres et brus
 * ne concerne pas le cadeau principal : parent → enfants (dans les
   cellules familiales)
 * le tirage au sort ne doit pas se faire au sein des cellules familiales
   (parents ↔ enfants, dans les deux sens)
 * le programme vérifie que tous reçoivent bien un cadeau
 * le programme vérifie que tous ont bien reçu le mail (accusé de réception)
 * la délégation des enfants jeunes à leurs parents doit respecter les
   règles précédentes
 * le programme doit vérifier que des flux de l'année précédente ne soit
   pas reproduit

Exemples de cas qui ne doivent pas se produire :

 * Anna (Fañch) > Nathalie
 * Nathalie > Fañch

"""
import logging
from dataclasses import dataclass
from pathlib import Path

from . import model, drawing, mailing, rules
from .types import Filepath, Flows

# ---- defaults
HERE = Path(__file__).parent
CWD = Path(".")  #: current working directory


@dataclass
class BaseNames:
    addresses_ffn: Path = CWD / "addresses.yaml"
    secrets_ffn: Path = CWD / "secrets.yaml"
    #: filename for new flows -- do not put year in it!
    flows_ffn: Path = CWD / "flows.yaml"


BASENAMES = BaseNames()


# ---- functions


def draw_gifts(
    history_dir: Path = CWD,
    addresses_ffn: Path = BASENAMES.addresses_ffn,
    secrets_ffn: Path = BASENAMES.secrets_ffn,
    flows_ffn: Path = BASENAMES.flows_ffn,
    send_mails: bool = False,
) -> tuple[model.Family, Flows]:
    """Build gifts repartition from family description file and send emails.

    Args:
        history_dir: folder containing `flows-YYYY.yaml` files, that
            provide previous years' drawings.
        addresses_ffn: family description, with emails, to be loaded using
            :py:meth:`Family.from_file`
        secrets_ffn: configuration to be used to send emails (in fact, to
            build a :py:class:Secrets instance)
        flows_ffn: filename where to write the built loops.
        send_mails: if `False` (default), emails are prepared but not sent.

    Returns:
        tuple of family instance and created flows.

    """
    logger = logging.getLogger("build-solution")
    # build check_not_in_YYYY functions
    rules.build_not_in_previous_year_rules(history_dir)

    # load family
    family = model.Family.from_file(email_ffn=addresses_ffn)
    # create the gift flows
    flows = drawing.shuffle(family.persons)
    # build the gift loops
    loops = drawing.build_loops(flows)
    # and report the obtained flows and loops
    logger.info(
        "found %i loops: %s",
        len(loops),
        drawing.repr_loops(loops, loops_sep="; "),
    )
    model.write_loops(loops, flows_ffn)
    logging.info("loops written to '%s'", flows_ffn)

    # send emails
    secrets = model.Secrets.from_file(secrets_ffn)
    emails = mailing.build_emails(family.all_names, flows)
    if send_mails:
        mailing.send_emails(emails, secrets)
    else:
        logger.info("emails option disabled; emails will not be sent")

    return family, flows


def send_reminder(
    addresses_ffn: Filepath,
    secrets_ffn: Filepath,
    flows_ffn: Filepath,
    send_mails: bool = False,
) -> tuple[model.Family, Flows]:
    """Reload flows and resend emails.

    Args:
        addresses_ffn: family description, with emails, to be loaded using
            :py:meth:`Family.from_file`
        secrets_ffn: configuration to be used to send emails (in fact, to
            build a :py:class:Secrets instance)
        flows_ffn: filename where the loops to load are stored.
        send_mails: if `False` (default), emails are prepared but not sent.

    Returns:
        family instance and load flows

    """
    logger = logging.getLogger("send-reminder")
    family = model.Family.from_file(email_ffn=addresses_ffn)
    loops = model.load_loops(flows_ffn)
    flows = drawing.flows_from_loops(loops)
    secrets = model.Secrets.from_file(secrets_ffn)
    # report
    logger.info(
        "found %i loops: %s",
        len(loops),
        drawing.repr_loops(loops, loops_sep="; "),
    )
    # build emails
    emails = mailing.build_emails(family.all_names, flows, reminder=True)
    if send_mails:
        mailing.send_emails(emails, secrets=secrets)

    return family, flows


def read_version():
    version_ffn = HERE / "VERSION"
    with open(version_ffn) as fh:
        return fh.read().strip()
