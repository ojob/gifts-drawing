from pathlib import Path
from typing import Union

Filepath = Union[str, Path]
Flows = dict[str, str]
Loops = list[list[str]]
