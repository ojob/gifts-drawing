import logging
from pathlib import Path

from . import model, drawing
from .model import Person
from .types import Flows

#: Registered checks in this module.
CHECKS = []


#: Decorator to populate :py:const:`CHECKS` list.
def register(check_func):
    CHECKS.append(check_func)
    return check_func


@register
def no_gift_to_self(senders: list[Person], receivers: list[Person]) -> bool:
    logger = logging.getLogger("check_no-gift-to-self")
    for sender, receiver in zip(senders, receivers):
        if sender is receiver:
            logger.debug("%s", sender.name)
            return False
    logger.debug("ok")
    return True


@register
def not_in_same_sub(senders: list[Person], receivers: list[Person]) -> bool:
    logger = logging.getLogger("check_not-in-same-sub")
    for sender, receiver in zip(senders, receivers):
        if sender.in_same_sub(receiver):
            logger.debug("%s, %s", sender.name, receiver.name)
            return False
    logger.debug("ok")
    return True


def build_not_in_previous_year_rule(year: str, flows: Flows):
    """Build a rule to check that *flows* is not similar to a previous year."""

    @register
    def not_in_previous_year(
        senders: list[Person],
        receivers: list[Person],
    ) -> bool:
        logger = logging.getLogger(f"check_not-in-year-{year}")
        for sender, receiver in zip(senders, receivers):
            if sender.name in flows and flows[sender.name] == receiver.name:
                logger.debug("%s, %s", sender.name, receiver.name)
                return False
        logger.debug("ok")
        return True


def build_not_in_previous_year_rules(folder: Path = Path(".")):
    """Build the previous rule, for every loops file found in *folder*."""
    for flow_ffn in folder.glob("flow-*.yaml"):
        year = flow_ffn.name[len("flow-") : -len(".yaml")]
        loops = model.load_loops(flow_ffn)
        flows = drawing.flows_from_loops(loops)
        logging.debug("found flows for %s: %s", year, flow_ffn.name)
        build_not_in_previous_year_rule(year=year, flows=flows)
