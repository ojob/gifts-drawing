"""Module providing the command-line interface."""

import logging
from pathlib import Path

import click

from . import draw_gifts, read_version, BASENAMES, CWD


@click.command()
@click.option(
    "-h",
    "--history",
    "history_dir",
    default=CWD,
    type=click.Path(exists=True, file_okay=False, path_type=Path),
)
@click.option(
    "-a",
    "--addresses",
    "addresses_ffn",
    default=BASENAMES.addresses_ffn,
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
)
@click.option(
    "-s",
    "--secrets",
    "secrets_ffn",
    default=BASENAMES.secrets_ffn,
    type=click.Path(exists=True, dir_okay=False, path_type=Path),
)
@click.option(
    "-f",
    "--flows",
    "flows_ffn",
    default=BASENAMES.flows_ffn,
    type=click.Path(dir_okay=False, writable=True, path_type=Path),
)
@click.option("--send-mails", is_flag=True, default=False)
@click.option("-v", "verbose", count=True)
@click.version_option(version=read_version())
def cli(
    history_dir: Path,
    addresses_ffn: Path,
    secrets_ffn: Path,
    flows_ffn: Path,
    send_mails: bool,
    verbose: int,
) -> None:
    """Command-line interface for drawing gifts."""
    if verbose == 2:
        logging.basicConfig(level=logging.DEBUG)
    elif verbose == 1:
        logging.basicConfig(level=logging.INFO)
    else:
        logging.basicConfig(level=logging.WARNING)

    draw_gifts(
        history_dir=history_dir,
        addresses_ffn=addresses_ffn,
        secrets_ffn=secrets_ffn,
        flows_ffn=flows_ffn,
        send_mails=send_mails,
    )
