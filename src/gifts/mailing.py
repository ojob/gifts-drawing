"""This module provides the functions related to e-mailing."""

import logging
import smtplib
import time
from email.header import Header
from email.mime.text import MIMEText
from typing import Dict, List

from .types import Flows
from .model import Person, Secrets


def build_emails(
    persons: Dict[str, Person],
    flows: Flows,
    reminder: bool = False,
) -> List[MIMEText]:
    """Create emails from *senders* to *receivers*."""
    # mails preparation
    msgs = []
    reminder_str = "[rappel] " if reminder else ""
    for sender_name, sender in persons.items():
        receiver = persons[flows[sender.name]]
        msgs.append(build_email(sender, receiver, reminder_str))

    check_emails_senders(persons, msgs)  # may raise ValueError
    return msgs


def build_email(
    sender: Person,
    receiver: Person,
    reminder_str: str,
) -> MIMEText:
    """Create one email."""
    subject = (
        f"{reminder_str}Cadeau de Noël : résultat du tirage au "
        f"sort pour {sender.name} !"
    )
    body = (
        f"Résultat du tirage au sort pour {sender.name} : tu dois "
        f"faire un cadeau à {receiver.name} ({receiver.email}) !"
    )
    msg = MIMEText(body, "plain", "utf-8")
    msg["Subject"] = Header(subject, "utf-8")
    msg["To"] = sender.email
    return msg


def check_emails_senders(
    persons: Dict[str, Person],
    emails: List[MIMEText],
) -> None:
    """Check that an email will be sent to every person.

    Raises:
        ValueError: if some persons are not present as senders.

    """
    senders_emails = [email["To"] for email in emails]
    persons_with_no_assignation = set(
        person.email for person in persons.values()
    ) - set(senders_emails)
    if persons_with_no_assignation:
        raise ValueError(
            "certaines personnes n'ont pas reçu d'assignation: "
            f"{', '.join(sorted(persons_with_no_assignation))}"
        )


def send_emails(msgs: List[MIMEText], secrets: Secrets) -> None:
    """Send *msgs* as emails."""
    logging.debug("connecting to smtp...")
    with smtplib.SMTP_SSL(secrets.SMTP_SERVER, port=secrets.SMTP_PORT) as smtp:
        smtp.login(secrets.FROM, secrets.PASSWORD)
        logging.debug("logged in")

        for msg_idx, msg in enumerate(msgs):
            msg["From"] = secrets.FROM
            logging.debug(f"sending message {msg_idx + 1}/{len(msgs)}...")
            send_email(msg, smtp)
            time.sleep(1)  # give some fresh air to the SMTP server

    logging.debug("all emails sent.")


def send_email(
    msg: MIMEText,
    smtp: smtplib.SMTP,
    _trials: int = 0,
) -> None:
    """Send one *msg* as email, and tries again if necessary.

    Args:
        msg: email to be sent
        smtp: server connection. Better use an SMTP_SSL one!
        _trials: counter of the retries, to lengthen the delay between trials.

    """
    # basic validation before sending
    if "@" not in msg["To"]:
        raise ValueError(f"invalid email address: {msg['To']}")
    logging.debug("sending email to %s", msg["To"])
    try:
        smtp.sendmail(msg["From"], msg["To"], msg.as_string())
    except (smtplib.SMTPRecipientsRefused, smtplib.SMTPDataError) as exc:
        if _trials < 100:
            delay = 3 ** _trials
            logging.warning(
                "Could not send email, trying again in %d seconds: %s",
                delay,
                exc,
            )
            time.sleep(delay)
            send_email(msg, smtp, _trials + 1)


def demo_uft8_mail():
    """A working email sending demo.

    Credits: https://stackoverflow.com/a/8173543/736151

    """
    secrets = Secrets.from_file("secrets.yaml")
    with smtplib.SMTP_SSL(secrets.SMTP_SERVER, port=secrets.SMTP_PORT) as s:
        s.login(secrets.FROM, secrets.PASSWORD)

        subject = "À un Joël"
        text = "Du texte, avec accents ça devrait passer à Rémy"

        msg = MIMEText(text, "plain", "utf-8")
        msg["From"] = secrets.FROM
        msg["To"] = secrets.FROM
        msg["Subject"] = Header(subject, "utf-8")
        try:
            s.sendmail(secrets.FROM, msg["To"], msg.as_string())
            print(f"sent email to {msg['To']}")
        except Exception as e:
            print(f"cannot send email to {msg['To']}: {e}")
