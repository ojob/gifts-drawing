"""This module provides the model of this app."""
from __future__ import annotations

import dataclasses
from typing import Union

import yaml

from .types import Filepath, Loops


@dataclasses.dataclass
class Secrets:
    """Class to hold secrets for email sending."""

    FROM: str
    SMTP_SERVER: str
    SMTP_PORT: int
    ACCOUNT: str
    PASSWORD: str

    @staticmethod
    def from_file(file_ffn: Filepath) -> Secrets:
        with open(file_ffn) as fh_in:
            data = yaml.safe_load(fh_in)
        return Secrets(**data)


@dataclasses.dataclass
class Person:
    """Class to define a person and their attributes."""

    name: str
    email: str
    family: Union[None, Family] = dataclasses.field(default=None, repr=False)

    def in_same_sub(self, other: Person) -> bool:
        """Return `True` if *name1* and *name2* are in same sub-family.

        >>> family = Family.from_file('addresses-test.yaml')
        >>> family['Camille'].in_same_sub(family['Joël'])
        True
        >>> family['Rémy'].in_same_sub(family['Joël'])
        False
        >>> family['joe'].in_same_sub(family['bob'])
        False
        >>> family['joe'].in_same_sub(family['Raphaëlle'])
        False

        """
        return self.family is other.family


@dataclasses.dataclass
class Family:
    """Class to hold a family definition, with sub-families.

    >>> family = Family.from_file('addresses-test.yaml')
    >>> family['Camille']
    Person(name='Camille', email='joel@bourgault.fr')
    >>> 'Joël' in family
    True
    >>> family['Joël'].family.name
    'Joël & co'

    """

    name: str
    people: dict[str, Person] = dataclasses.field(default_factory=dict)
    subs: dict[str, Family] = dataclasses.field(default_factory=dict)
    all_names: dict[str, Person] = dataclasses.field(
        default_factory=dict, repr=False
    )

    def __post_init__(self) -> None:
        """Take care to initialize all instance attributes correctly."""
        self.add_persons(*self.people.values())
        for sub_fam in self.subs.values():
            self.add_persons(*sub_fam.all_names.values())

    # ---- magic methods and special getters ------------------------------
    def __getitem__(self, name: str) -> Union[Person, Family]:
        """Return *name* from this instance.

        Raises:
            KeyError: if neither in this Family instance nor in any sub-family
                instance.

        """
        try:
            return self.get_person(name=name)
        except KeyError:
            return self.get_sub(name=name)

    def __contains__(self, name: str) -> bool:
        """Return whether *name* is a known :py:class:Person or sub-family."""
        return name in self.all_names or name in self.subs

    def get_person(self, name: str) -> Person:
        """Return the person corresponding to *name*."""
        return self.all_names[name]

    def get_sub(self, name: str) -> Family:
        """Return the sub-family corresponding to *name*."""
        return self.subs[name]

    # ---- filling methods ------------------------------------------------
    def add_persons(self, *persons: Person) -> None:
        """Add multiple :py:class:Person in :py:attr:all_names.

        Raises:
            ValueError: if one name is found in :py:attr:all_names

        >>> family = Family(
        ...     name='root', people={'bob': Person(name='bob', email='')})
        >>> 'bob' in family.all_names
        True
        >>> family.add_persons(Person(name='bob', email='doc'))
        Traceback (most recent call last):
        ...
        ValueError: duplicate name: bob

        """
        for person in persons:
            if person.name in self.all_names:
                raise ValueError(f"duplicate name: {person.name}")
            self.all_names[person.name] = person

    @property
    def persons(self) -> list[Person]:
        return list(self.all_names.values())

    # ---- building methods -----------------------------------------------
    @staticmethod
    def from_dict(family_data: dict, name: str = "root") -> Family:
        """Load Family from a dictionary."""
        family = Family(name=name)
        for k, v in family_data.items():
            if isinstance(v, dict):  # create new sub-family
                sub_fam = Family.from_dict(name=k, family_data=v)
                family.subs[sub_fam.name] = sub_fam
                family.add_persons(*sub_fam.all_names.values())
            else:
                person = Person(name=k, email=v, family=family)
                family.people[person.name] = person
                family.add_persons(person)
        return family

    @staticmethod
    def from_file(email_ffn: Filepath) -> Family:
        """Load family structure and adresses provided in *email_ffn*.

        Args:
            email_ffn: path to a YAML file.

        Returns:
            Family: structure.

        >>> family = Family.from_file('addresses.yaml')
        >>> family.name
        'root'
        >>> len(family.subs)
        4
        >>> list(family.subs['Joël & co'].all_names.keys())
        ['Joël', 'Camille']

        """
        with open(email_ffn) as fh_in:
            data = yaml.safe_load(fh_in)
            return Family.from_dict(data)


def load_loops(loops_ffn: Filepath) -> Loops:
    """Load loops from YAML file."""
    with open(loops_ffn) as fh_in:
        loops = yaml.safe_load(fh_in)["loops"]
    return loops


def write_loops(loops: Loops, out_ffn: Filepath) -> None:
    """Write loops to YAML file."""
    with open(out_ffn, "wb") as fh_out:
        fh_out.write(yaml.dump({"loops": loops}, encoding="utf-8"))
