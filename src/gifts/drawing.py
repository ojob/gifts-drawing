"""This module holds the functions related to drawing and related objects."""

import logging
import random
import typing
from typing import Any

from .model import Person
from .rules import CHECKS
from .types import Flows, Loops


def gen_candidate(seq: typing.Sequence[Any]) -> list[Any]:
    candidate = random.sample(seq, k=len(seq))
    return candidate


def shuffle(senders: list[Person]) -> Flows:
    """Return a shuffled list from *seq*, ensuring all items have moved."""
    receivers = gen_candidate(senders)
    draws_trials_nb = 0
    while not all(check(senders, receivers) for check in CHECKS):
        receivers = gen_candidate(senders)
        draws_trials_nb += 1
        if draws_trials_nb % 10 ** 3 == 0:
            logging.warning(
                "found no adequate candidate after %s shuffles", draws_trials_nb
            )
    return {
        sender.name: receiver.name
        for sender, receiver in zip(senders, receivers)
    }


def build_loops(flows: Flows) -> Loops:
    """Return sequences of flows."""
    persons = list(flows.keys())  # use a copy to pop-up people
    loops = []
    while persons:  # this takes care of possible loops
        loop = []
        sender = persons.pop()
        loop.append(sender)
        receiver = flows[sender]
        while receiver != sender:  # look for end of loop
            loop.append(receiver)
            persons.remove(receiver)
            receiver = flows[receiver]
        loop.append(sender)
        loops.append(loop)
    return loops


def repr_loops(
    loops: Loops,
    loops_sep: str = "\n",
    loop_sep: str = " > ",
) -> str:
    """Build string for representing loops of flows."""
    return loops_sep.join(
        loop_sep.join(person for person in loop) for loop in loops
    )


def flows_from_loops(loops: Loops) -> Flows:
    """Build flows from the loops."""
    flows: Flows = {}
    for loop in loops:
        for sender_name, receiver_name in zip(loop[:-1], loop[1:]):
            if sender_name in flows:
                raise ValueError(f"duplicate sender_name: {sender_name}")
            flows[sender_name] = receiver_name
    return flows
