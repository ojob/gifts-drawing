from pathlib import Path
from setuptools import setup, find_packages


PACKAGE_NAME = "gifts"

VERSION_FFN = version_ffn = (
    Path(__file__).parent / "src" / PACKAGE_NAME / "VERSION"
)


def read_version(version_ffn=VERSION_FFN):
    with open(version_ffn) as fh:
        return fh.read().strip()


setup(
    name=PACKAGE_NAME,
    version=read_version(),
    description="Small script to compute gifts repartition and send mail",
    packages=find_packages("src"),
    package_dir={"": "src"},
    install_requires=[
        "click",
        "pyyaml",
    ],
    entry_points="""
        [console_scripts]
        gifts=gifts.cli:cli
    """
)
