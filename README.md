# Gifts Random Drawing

Simple tool to handle random drawing of gifts inside a family, with following
features:

- definition of family members in a dedicated file,
- random drawing between people, taking care to avoid drawings inside
  sub-families,
- history of gifts drawings, in order to avoid repetitions.

This is pretty close to [`santa`][1].

## Prerequisites

- Python >= 3.6

## Installation

[available soon!] When the package is on PyPI, installation is as simply as:

```
pip install gifts-drawing 
```

Which installs the `gifts` command-line tool.

## Usage

1. Build the file defining the family: `addresses.yaml`
2. Prepare the settings for email sending: `secrets.yaml`
3. Launch the tool

## Options

See `gifts --help` for more details.
